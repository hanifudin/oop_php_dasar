<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

// animal

$animal1 = new animal("Shaun");

echo $animal1->get_name(); echo "<br>";
echo $animal1->get_legs(); echo "<br>";
// echo $animal1->get_cold_blooded();
echo var_dump($animal1->cold_blooded); echo "<br>";

echo "<br>";
echo "<br>";

// frog

$kodok = new Frog("buduk");
echo $kodok->get_name() ; echo "<br>";
echo $kodok->get_jump() ; echo "<br>";

echo "<br>";
echo "<br>";


// ape
$sungokong = new Ape("kera sakti");
echo $sungokong->get_name() ; echo "<br>";
echo $sungokong->get_yell() ; echo "<br>";

?>